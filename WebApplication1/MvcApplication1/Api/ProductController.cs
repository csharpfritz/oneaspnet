﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData.Query;
using MvcApplication1.Product;

namespace MvcApplication1.Api
{

  public class ProductController : ApiController
  {

[Queryable]
public IQueryable<dynamic> Get(ODataQueryOptions options)
{

  return Controllers.ProductController._Products.Select(b => new
  {
    Id = b.Id,
    Name = b.Name,
    NumInStock = b.NumInStock,
    Price = b.Price.ToString("$0.00")
  }).AsQueryable();
      
}

  }

}
