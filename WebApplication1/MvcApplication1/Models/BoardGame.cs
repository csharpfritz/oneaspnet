using System.ComponentModel.DataAnnotations;

namespace MvcApplication1.Models
{
public class BoardGame
{

  public int Id { get; set; }

  public string Name { get; set; }

  [DisplayFormat(DataFormatString="$0.00")]
  public decimal Price { get; set; }

  [Display(Name="Number of items in stock"), Range(0,10000)]
  public int NumInStock { get; set; }

}
}