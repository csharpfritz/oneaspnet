﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Search.Mobile.aspx.cs" Inherits="MvcApplication1.Product.Search_Mobile" MasterPageFile="~/Site.Mobile.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
<style>
    * {
        font-family: Arial, 'DejaVu Sans', 'Liberation Sans', Freesans, sans-serif;
    }

    ul {
        margin: 0px;
        padding: 0px;
        width: 100%;
    }
    li {
        list-style-type: none;
        padding: 3px 2px;
        border-bottom: 1px solid #333;
    }


</style>
<telerik:RadScriptManager runat="server" ID="scriptMgr"></telerik:RadScriptManager>
<telerik:RadListView runat="server" ID="listView" ItemType="MvcApplication1.Models.BoardGame" SelectMethod="GetGames" ItemPlaceholderID="container">

    <LayoutTemplate>
        <ul style="margin-top: 70px;">
            <asp:PlaceHolder runat="server" ID="container"></asp:PlaceHolder>
        </ul>
    </LayoutTemplate>

    <ItemTemplate>
        <li>
        <b>Name:</b><%#: Item.Name %><br />
        <b># in Stock:</b><%#: Item.NumInStock %>
        </li>
    </ItemTemplate>

</telerik:RadListView>

</asp:Content>