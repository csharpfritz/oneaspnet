﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Search.aspx.cs" 
  Inherits="MvcApplication1.Product.Search" 
  MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content runat="server" id="main" ContentPlaceHolderID="body">

  <telerik:RadGrid ID="searchProducts" runat="server" width="400"
    AllowFilteringByColumn="True" CellSpacing="0" GridLines="None"
    AllowSorting="True" AutoGenerateColumns="false" EnableViewState="false"
    Skin="Telerik">
        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True" ClientEvents-OnGridCreated="GridCreated">
            <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
            <DataBinding Location="/api" ResponseType="JSON">
                <DataService TableName="Product" Type="OData"  />
            </DataBinding>
        </ClientSettings>
        <MasterTableView ClientDataKeyNames="Id" DataKeyNames="Id">
            <Columns>
                <telerik:GridBoundColumn DataField="Id" HeaderStyle-Width="0" ItemStyle-Width="0"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Name" HeaderText="Name" HeaderStyle-Width="120" ItemStyle-Width="120"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ItemStyle-CssClass="gridPrice" DataField="Price" HeaderText="Price" ItemStyle-HorizontalAlign="Right"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="NumInStock" ItemStyle-CssClass="numInStock" HeaderText="# in Stock"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>

    <script src="/Scripts/jquery.signalR-1.0.1.min.js"></script>
<script src="/signalr/hubs"></script>
<script type="text/javascript">

  var grid;

  function GridCreated(sender, eventArgs) {
      grid = sender;
  }

  function GetRow(id) {

      var items = grid.get_masterTableView().get_dataItems();

      for (var i = 0; i < items.length; i++) {
          var $thisItem = $(items[i].get_element());
          if ($thisItem.find("TD:first").text() == id) {
              return $thisItem;
          }
      }

  }

  $().ready(function() {
            
      var stockWatcher = $.connection.stockHub;

      stockWatcher.client.setNewStockLevel = function(id, newValue) {

        var row = GetRow(id);

        var orgColor = row.css("background-color");

        row.find(".numInStock").animate({
          backgroundColor: "#FFEFD5"
        }, 2000, "swing", function () {
          row.find(".numInStock").html(newValue).animate({
            backgroundColor: orgColor
          }, 2000)
        });

      };

      $.connection.hub.start();

  })

</script>
</asp:Content>