using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
  public static class ControllerExtensions
  {
public static ViewResult RazorView(this Controller controller, string viewName = null, object model = null)
{
        
  if (model != null)
    controller.ViewData.Model = model;

  controller.ViewBag._ViewName = !string.IsNullOrEmpty(viewName)
          ? viewName
          : controller.RouteData.GetRequiredString("action");

  return new ViewResult
  {
    ViewName = "RazorView",
    ViewData = controller.ViewData,
    TempData = controller.TempData
  };

}
    
  }
}