﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;
using MvcApplication1.Product;

namespace MvcApplication1.Controllers
{
    public class ProductController : Controller
    {
        //
        // GET: /Product/

        public static readonly List<BoardGame> _Products = new List<BoardGame>()
        {
            new BoardGame()
            {
                Id = 1,
                Name = "Chess",
                Price = 9.99M
            },
            new BoardGame()
            {
                Id = 2,
                Name = "Checkers",
                Price = 7.99M
            },
            new BoardGame()
            {
                Id = 3,
                Name = "Battleship",
                Price = 8.99M
            },
            new BoardGame()
            {
                Id = 4,
                Name = "Backgammon",
                Price = 12.99M
            },
            new BoardGame()
            {
                Id = 5,
                Name = "Dominoes",
                Price = 15.95M
            },
            new BoardGame()
            {
                Id = 6,
                Name = "Mah Jongg",
                Price = 35.95M
            },
            new BoardGame()
            {
                Id = 7,
                Name = "Go",
                Price = 29.99M
            }
        };
    
        public ActionResult New()
        {
            return this.RazorView();
        }

        [HttpPost]
        public ActionResult New(BoardGame newGame)
        {
            if (!ModelState.IsValid)
            {
                return this.RazorView();
            }

            newGame.Id = _Products.Count + 1;
            _Products.Add(newGame);
      
            return Redirect("~/Product/Search");

        }

    }

}
