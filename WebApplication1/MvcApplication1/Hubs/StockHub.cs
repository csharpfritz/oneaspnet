﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Web;
using Microsoft.AspNet.SignalR;
using MvcApplication1.Controllers;

namespace MvcApplication1.Hubs
{

public class StockHub : Hub
{

  public static readonly Timer _Timer = new Timer();
  private static readonly Random _Rdm = new Random();

    private static int _ClientCount = 0;

  static StockHub()
  {
      
    _Timer.Interval = 2000;
    _Timer.Elapsed += _Timer_Elapsed;
    _Timer.Start();
  }

  static void _Timer_Elapsed(object sender, ElapsedEventArgs e)
  {

    var products = ProductController._Products;
    var p = products.Skip(_Rdm.Next(0, products.Count())).First();
      
    var newStockLevel = p.NumInStock + _Rdm.Next(-1 * p.NumInStock, 100);
    p.NumInStock = newStockLevel;

    var hub = GlobalHost.ConnectionManager.GetHubContext<StockHub>();
    hub.Clients.All.setNewStockLevel(p.Id, newStockLevel);

  }

  
}

}